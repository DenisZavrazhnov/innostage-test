package ru.innostage.test.service;

import ru.innostage.test.dto.UserDto;

public interface UserService {
    UserDto getUserInfo(long id);
}
