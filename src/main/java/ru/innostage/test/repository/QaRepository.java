package ru.innostage.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.innostage.test.models.QA;

@Repository
public interface QaRepository extends JpaRepository<QA, Long> {
}
