package ru.innostage.test.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.innostage.test.repository.QaRepository;
import ru.innostage.test.service.QaService;

@Service
@RequiredArgsConstructor
public class QaServiceImpl implements QaService {
    private final QaRepository qaRepository;
}
