package ru.innostage.test.service.impl;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.innostage.test.dto.SignUpDto;
import ru.innostage.test.dto.TokenDto;
import ru.innostage.test.models.User;
import ru.innostage.test.repository.UserRepository;
import ru.innostage.test.service.SignInService;

import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SignInServiceImpl implements SignInService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${jwt.secret}")
    private String token;

    @Value("${jwt.expirationDateInMs}")
    private String jwtExpirationDateInMs;

    @Override
    public TokenDto signIn(SignUpDto sign) {
        Optional<User> user = userRepository.findUsersByUsername(sign.getUsername());
        if (user.isEmpty()) {
            throw new IllegalArgumentException("User with this username was not found");
        }
        if (!passwordEncoder.matches(sign.getPassword(), user.get().getPassword())) {
            throw new IllegalArgumentException("Wrong password");
        }

        return TokenDto.builder()
                .token(Jwts.builder()
                        .setSubject(String.valueOf(user.get().getId()))
                        .claim("name", user.get().getUsername())
                        .claim("role", user.get().getRole())
                        .signWith(SignatureAlgorithm.HS256, token)
                        .setExpiration(new Date(System.currentTimeMillis() + Long.parseLong(jwtExpirationDateInMs)))
                        .compact())
                .build();
    }
}
