package ru.innostage.test.controllers.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.innostage.test.dto.ResponseDto;

import java.time.Instant;

@RestControllerAdvice
public class AdviceController {
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseDto handleIllegalArgumentException(IllegalArgumentException e) {
        return new ResponseDto(e.getMessage(), Instant.now().getEpochSecond());
    }
}
