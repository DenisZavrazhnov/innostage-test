package ru.innostage.test.service;

import ru.innostage.test.dto.ResponseDto;
import ru.innostage.test.dto.SignUpDto;

public interface SignUpService {
    ResponseDto registerNewUser(SignUpDto signUpDto);
}
