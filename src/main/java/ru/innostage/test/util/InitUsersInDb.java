package ru.innostage.test.util;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.innostage.test.models.Role;
import ru.innostage.test.models.User;
import ru.innostage.test.repository.UserRepository;

import java.time.Instant;
import java.util.List;

@Component
@RequiredArgsConstructor
public class InitUsersInDb {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @EventListener(ApplicationReadyEvent.class)
    public void createUsersAndTests() {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
            User admin = User.builder()
                    .dateOfCreate(Instant.now().getEpochSecond())
                    .maxPointOfPassedTests(0)
                    .username("admin")
                    .password(passwordEncoder.encode("qwerty007"))
                    .role(Role.ADMIN)
                    .testCounter(0)
                    .build();
            userRepository.save(admin);
        }
    }

}
