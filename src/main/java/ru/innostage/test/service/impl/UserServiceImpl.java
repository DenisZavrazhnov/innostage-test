package ru.innostage.test.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.innostage.test.dto.UserDto;
import ru.innostage.test.models.Role;
import ru.innostage.test.models.User;
import ru.innostage.test.repository.UserRepository;
import ru.innostage.test.service.UserService;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public UserDto getUserInfo(long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new IllegalArgumentException("User with id = " + id + " not found");
        }
        return UserDto.builder()
                .id(user.get().getId())
                .username(user.get().getUsername())
                .dateOfCreate(user.get().getDateOfCreate())
                .testCounter(user.get().getTestCounter())
                .sumOfPoints(user.get().getSumOfPoints())
                .maxPointOfPassedTests(user.get().getMaxPointOfPassedTests())
                .isAdmin(isAdminUser(user.get()))
                .build();
    }

    private boolean isAdminUser(User user) {
        return (user.getRole() == Role.ADMIN);
    }


}
