package ru.innostage.test.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.innostage.test.config.jwt.UserDetailsImpl;
import ru.innostage.test.dto.ResponseDto;
import ru.innostage.test.dto.SignUpDto;
import ru.innostage.test.dto.TokenDto;
import ru.innostage.test.dto.UserDto;
import ru.innostage.test.service.SignInService;
import ru.innostage.test.service.SignUpService;
import ru.innostage.test.service.UserService;

import java.time.Instant;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final SignInService signInService;
    private final SignUpService signUpService;

    @PostMapping("/signup")
    public ResponseEntity<ResponseDto> registration(@RequestBody SignUpDto sign) {
        signUpService.registerNewUser(sign);
        return ResponseEntity.ok(new ResponseDto("User: " + sign.getUsername() + " registered successfully", Instant.now().getEpochSecond()));
    }

    @PostMapping("/signin")
    public ResponseEntity<TokenDto> signIn(@RequestBody SignUpDto sign) {
        return ResponseEntity.ok(signInService.signIn(sign));
    }

    @GetMapping("/info")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<UserDto> getUserInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getDetails();
        return ResponseEntity.ok(userService.getUserInfo(userDetails.getUserId()));
    }
}
