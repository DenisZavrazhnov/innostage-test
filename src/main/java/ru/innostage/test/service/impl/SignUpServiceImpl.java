package ru.innostage.test.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.innostage.test.dto.ResponseDto;
import ru.innostage.test.dto.SignUpDto;
import ru.innostage.test.models.Role;
import ru.innostage.test.models.User;
import ru.innostage.test.repository.UserRepository;
import ru.innostage.test.service.SignUpService;

import java.time.Instant;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public ResponseDto registerNewUser(SignUpDto signUpDto) {
        if (signUpDto.getUsername() == null && signUpDto.getPassword() == null) {
            throw new IllegalArgumentException("Invalid username or password");
        }
        Optional<User> users = userRepository.findUsersByUsername(signUpDto.getUsername());
        if (users.isPresent()) {
            throw new IllegalArgumentException("User with this login is already registered");
        }
        userRepository.save(User.builder()
                .username(signUpDto.getUsername())
                .password(passwordEncoder.encode(signUpDto.getPassword()))
                .role(Role.USER)
                .dateOfCreate(Instant.now().getEpochSecond())
                .sumOfPoints(0)
                .build());
        return ResponseDto.builder()
                .responseMessage("User " + signUpDto.getUsername() + " registered successfully")
                .currentTime(Instant.now().getEpochSecond())
                .build();
    }
}
