$(document).ready(function () {
    const token = window.localStorage.getItem("token")
    console.log(token)
    if (token) {
        $.ajax({
            url: 'http://localhost:8080/api/user/info',
            type: 'GET',
            headers: {
                Authorization: token
            },
            success(response) {
                if (response.admin) {
                    $('#goToTestCreateButton').css('visibility', 'visible')
                }
            },
            error(e) {
                console.error(e)
                window.location.href = "/login.html"
            }
        });
        $.ajax({
            url: 'http://localhost:8080/api/test/list',
            type: 'GET',
            headers: {
                Authorization: token
            },
            success(response) {
                console.log(response)
                for (let i = 0; i < response.length; i++) {
                    const row = `<tr></tr><td class="listItem" id="test${response[i].id}">${response[i].title}</td></tr>`
                    $('#table').append(row)
                    $(`#test${response[i].id}`).click(function () {
                        window.location.href = `/test.html?id=${response[i].id}`
                    })
                }

            },
            error(e) {
                console.error(e)
                window.location.href = "/login.html"
            }
        })
    } else {
        window.location.href = "/login.html"
    }

});

$('#logout').click(function () {
    window.localStorage.clear()
    window.location.href = "/login.html"
})