package ru.innostage.test.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.innostage.test.dto.AnswerDto;
import ru.innostage.test.dto.QaDto;
import ru.innostage.test.dto.ResponseDto;
import ru.innostage.test.dto.TestDto;
import ru.innostage.test.models.Answer;
import ru.innostage.test.models.QA;
import ru.innostage.test.models.Test;
import ru.innostage.test.models.User;
import ru.innostage.test.repository.AnswerRepository;
import ru.innostage.test.repository.QaRepository;
import ru.innostage.test.repository.TestRepository;
import ru.innostage.test.repository.UserRepository;
import ru.innostage.test.service.TestService;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TestServiceImpl implements TestService {
    private final TestRepository testRepository;
    private final UserRepository userRepository;
    private final QaRepository qaRepository;
    private final AnswerRepository answerRepository;

    @Override
    public Test getTest(long id) {
        Optional<Test> optionalTest = testRepository.findById(id);
        if (optionalTest.isEmpty()) {
            throw new IllegalArgumentException("Incorrect test id");
        }
        List<QA> qaList = optionalTest.get().getQaList();
        for (QA qa : qaList) {
            Collections.shuffle(qa.getAnswers());
        }
        optionalTest.get().setQaList(qaList);
        Collections.shuffle(optionalTest.get().getQaList());
        return optionalTest.get();
    }

    @Override
    public ResponseDto createTest(TestDto testDto) {
        if (testDto == null) {
            throw new IllegalArgumentException("TestDto is null");
        }
        if (testDto.getTitle().equals("") || testDto.getTitle() == null) {
            throw new IllegalArgumentException("Test title can`t be null!!!");
        }
        Optional<Test> testFromDb = testRepository.findByTitle(testDto.getTitle());
        if (testFromDb.isPresent()) {
            throw new IllegalArgumentException("A test with the same name already exists");
        }
        Test test = Test.builder()
                .title(testDto.getTitle())
                .qaList(getQAList(testDto.getQuestions()))
                .build();
        testRepository.save(test);
        return new ResponseDto("Test created successfully", Instant.now().getEpochSecond());
    }

    @Override
    public List<TestDto> getTestList(long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isEmpty()) {
            throw new IllegalArgumentException("User with id " + userId + " not found");
        }
        List<Test> passedTest = user.get().getPassedTests();
        List<Test> testList = testRepository.findAll();
        testList.removeIf(test -> passedTest.stream().anyMatch(m -> m.getTitle().equals(test.getTitle())));

        List<TestDto> testDtoList = new ArrayList<>();
        for (Test t : testList) {
            testDtoList.add(TestDto.builder()
                    .id(t.getId())
                    .title(t.getTitle())
                    .build());
        }
        return testDtoList;
    }

    @Override
    public long getMaxPointsForAllTests() {
        long i = 0;
        List<Test> all = testRepository.findAll();
        for (Test test : all) {
            List<QA> allQA = test.getQaList();
            for (QA qa : allQA) {
                if (qa.isSingleChoice()) {
                    i += 1;
                } else {
                    List<Answer> answers = qa.getAnswers();
                    for (Answer answer : answers) {
                        if (answer.isCorrect()) {
                            i += 3;
                        }
                    }
                }
            }
        }
        return i;
    }

    @Override
    public ResponseDto submitTest(long userId, TestDto testDto) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isEmpty()) {
            throw new IllegalArgumentException("User with id " + userId + " not found");
        }
        Optional<Test> test = testRepository.findById(testDto.getId());
        if (test.isEmpty()) {
            throw new IllegalArgumentException("Test with id " + testDto.getId() + " not found");
        }
        if (user.get().getPassedTests().contains(test.get())) {
            throw new IllegalArgumentException("That test already passed");
        }
        List<QaDto> qaDtoList = testDto.getQuestions();
        for (QaDto qaDto : qaDtoList) {
            Optional<QA> qa = qaRepository.findById(qaDto.getId());
            if (qa.isEmpty()) {
                throw new IllegalArgumentException("QA with id = " + qaDto.getId() + " not found");
            }
            List<Answer> answers = getAnswerListFromAnswerDto(qaDto.getAnswers());
            if (!qa.get().isSingleChoice()) {
                if (answers.stream().anyMatch(answer -> !answer.isCorrect())) {

                } else user.get().setSumOfPoints(user.get().getSumOfPoints() + (3L * answers.size()));
            } else {
                if (answers.get(0).isCorrect()) {
                    user.get().setSumOfPoints(user.get().getSumOfPoints() + 1);
                }
            }

        }
        List<Test> testList = user.get().getPassedTests();
        if (!testList.contains(test.get())) {
            testList.add(test.get());
        }
        user.get().setTestCounter(testList.size());
        user.get().setMaxPointOfPassedTests(maxPointsOfPassedTests(user.get().getId()));
        userRepository.save(user.get());
        return new ResponseDto("Test passed!", Instant.now().getEpochSecond());
    }

    private long maxPointsOfPassedTests(long userId) {
        long i = 0;
        Optional<User> user = userRepository.findById(userId);
        if (user.isEmpty()) {
            throw new IllegalArgumentException("User with id= " + userId + " not found");
        }
        List<Test> testList = user.get().getPassedTests();
        if (testList.isEmpty()) {
            throw new IllegalArgumentException("User did not pass tests");
        }
        for (Test test : testList) {
            List<QA> allQA = test.getQaList();
            for (QA qa : allQA) {
                if (qa.isSingleChoice()) {
                    i += 1;
                } else {
                    List<Answer> answers = qa.getAnswers();
                    for (Answer answer : answers) {
                        if (answer.isCorrect()) {
                            i += 3;
                        }
                    }
                }
            }
        }
        return i;
    }

    private List<QA> getQAList(List<QaDto> qaDtoList) {
        if (qaDtoList.isEmpty()) {
            throw new IllegalArgumentException("Question can`t be null!!!");
        }
        List<QA> qaList = new ArrayList<>();
        for (QaDto qaDto : qaDtoList) {
            if (qaDto.getQuestion().equals("") || qaDto.getQuestion() == null) {
                throw new IllegalArgumentException("Question can`t be null!!!");
            }
            qaList.add(QA.builder()
                    .question(qaDto.getQuestion())
                    .isSingleChoice(qaDto.isSingleChoise())
                    .answers(getAnswerList(qaDto.getAnswers()))
                    .build());
        }
        return qaList;
    }

    private List<Answer> getAnswerList(List<AnswerDto> answerDtoList) {
        if (answerDtoList.isEmpty()) {
            throw new IllegalArgumentException("Answer can`t be null!!!");
        }
        List<Answer> answers = new ArrayList<>();
        for (AnswerDto answerDto : answerDtoList) {
            if (answerDto.getAnswer().equals("") || answerDto.getAnswer() == null) {
                throw new IllegalArgumentException("Answer can`t be null!!!");
            }
            answers.add(Answer.builder()
                    .answer(answerDto.getAnswer())
                    .isCorrect(answerDto.isCorrect())
                    .build());
        }
        return answers;
    }

    private List<Answer> getAnswerListFromAnswerDto(List<AnswerDto> answerDtoList) {
        List<Answer> answers = new ArrayList<>();
        for (AnswerDto answerDto : answerDtoList) {
            answers.add(answerRepository.getById(answerDto.getId()));
        }
        return answers;
    }
}
