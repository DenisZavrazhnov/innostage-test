$('#form').submit(function (event) {
    event.preventDefault()
    const username = event.target.login.value
    const password = event.target.password.value
    console.log(username, password)
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/api/user/signin",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify({
            username, password
        }),
        success(response) {
            console.log(window)
            window.localStorage.setItem("token", response.token)
            window.location.href = "/main.html"
        }
    })
})
