package ru.innostage.test.service;

import ru.innostage.test.dto.SignUpDto;
import ru.innostage.test.dto.TokenDto;

public interface SignInService {
    TokenDto signIn(SignUpDto sign);
}
