package ru.innostage.test.service;

import ru.innostage.test.dto.ResponseDto;
import ru.innostage.test.dto.TestDto;
import ru.innostage.test.models.Test;

import java.util.List;

public interface TestService {
    Test getTest(long id);

    ResponseDto createTest(TestDto testDto);

    List<TestDto> getTestList(long userId);

    long getMaxPointsForAllTests();

    ResponseDto submitTest(long userId, TestDto testDto);
}
