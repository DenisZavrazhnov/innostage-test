$(document).ready(function () {
    const token = window.localStorage.getItem("token")
    const testId = window.location.href.replace(/^(.*)id=(\d+)/, '$2')
    if (token) {
        $.ajax({
            url: `http://localhost:8080/api/test/${testId}`,
            type: 'GET',
            headers: {
                Authorization: token
            },
            success(response) {
                $('#testname').text(response.title)
                const testForm = $('#testContainer')
                for (let i = 0; i < response.qaList.length; i++) {
                    testForm.append(`<fieldset id="testFormFieldSet${i}"></fieldset>`)
                    const fieldSet = $(`#testFormFieldSet${i}`)
                    fieldSet.append(`<legend>${response.qaList[i].question}</legend>`)
                    if (response.qaList[i].singleChoice) {
                        for (let g = 0; g < response.qaList[i].answers.length; g++) {
                            const question = `
<p>${response.qaList[i].answers[g].answer}</p>
<input title="${response.qaList[i].answers[g].answer}" id="question${response.qaList[i].id}answer${response.qaList[i].answers[g].id}" type="radio" value="${response.qaList[i].answers[g].id}" name="answer${response.qaList[i].id}">`
                            fieldSet.append(question)
                        }
                    } else {
                        for (let g = 0; g < response.qaList[i].answers.length; g++) {
                            const question = `
<p>${response.qaList[i].answers[g].answer}</p>
<input title="${response.qaList[i].answers[g].answer}" id="question${response.qaList[i].id}answer${response.qaList[i].answers[g].id}" type="checkbox" value="${response.qaList[i].answers[g].id}" name="answer${response.qaList[i].id}">`
                            fieldSet.append(question)
                        }
                    }
                }
                testForm.append(`<button type="submit">submit</button>`)
                testForm.submit(function (event) {
                    const regexp = /^(question)(\d+)(answer)(\d+)/
                    event.preventDefault()
                    const payload = []
                    $("form#testContainer :input").each(function () {
                        const input = $(this)[0];
                        if (input.checked) {
                            const questionId = input.id.replace(regexp, "$2")
                            const i = payload.findIndex(a => a.id === questionId)
                            if (i === -1) {
                                payload.push({id: questionId, answers: [{id: input.value}]})
                            } else {
                                payload[i].answers.push({id: input.value})
                            }
                        }
                    });
                    const ahahahSuperPayload = {
                        id: response.id,
                        questions: payload
                    }
                    $.ajax({
                        type: "POST",
                        url: `http://localhost:8080/api/test/submit`,
                        headers: {
                            "Content-Type": "application/json",
                            Authorization: token
                        },
                        data: JSON.stringify(ahahahSuperPayload),
                        success(response) {
                            window.location.href = "/main.html"
                        }
                    })
                })
            },
            error(e) {
                console.error(e)
                // window.localStorage.clear()
                // window.location.href = "/login.html"
            }
        });
    } else {
        window.location.href = "/login.html"
    }

});


