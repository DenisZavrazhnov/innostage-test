package ru.innostage.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class AnswerDto {
    private long id;
    private String answer;
    @JsonProperty("isCorrect")
    private boolean isCorrect;
}
