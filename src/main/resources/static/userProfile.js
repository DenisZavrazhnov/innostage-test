$(document).ready(function () {
    const token = window.localStorage.getItem("token")
    console.log(token)
    if (token) {
        $.ajax({
            url: 'http://localhost:8080/api/user/info',
            type: 'GET',
            headers: {
                Authorization: token
            },
            success(response) {

                $("#userInfo").html(`<div>Имя пользователя: ${response.username}</div> <div>Количество очков: ${response.sumOfPoints}</div> <div>Количество пройденных тестов: ${response.testCounter}</div> <div>Возможное максимальное количество очков за пройденные тесты: ${response.maxPointOfPassedTests}</div> <div>Дата регистрации: ${new Date(response.dateOfCreate * 1000).toLocaleString()}</div>`)
            },
            error(e) {
                console.error(e)
                window.location.href = "/login.html"
            }
        });
    } else {
        window.location.href = "/login.html"
    }

});