package ru.innostage.test.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userId")
    private long id;

    private String username;
    private String password;
    private long sumOfPoints;
    private long dateOfCreate;
    private long testCounter;
    private long maxPointOfPassedTests;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Test> passedTests;

    @Enumerated(value = EnumType.STRING)
    private Role role;
}
