package ru.innostage.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class QaDto {
    private long id;
    private long testId;
    private String question;
    private List<AnswerDto> answers;
    @JsonProperty("isSingleChoise")
    private boolean isSingleChoise;
}
