let howMuchQuestions = 1
$(document).ready(function () {
    const token = window.localStorage.getItem("token")
    if (!token) {
        window.location.href = "/login.html"
    }
})


$('#addAnotherQuestion').click(function () {
    howMuchQuestions++
    let question = `<div>
        <p>Вопрос</p>
        <input aria-label="question${howMuchQuestions}" type="text" name="question${howMuchQuestions}"/>
        <p>Правильный ответ (если их несколько, то через запятую)</p>
        <input aria-label="answerTrue${howMuchQuestions}" type="text" name="answerTrue${howMuchQuestions}"/>
        <p>Неправильные ответы (через запятую)</p>
        <input aria-label="answerFalse${howMuchQuestions}" type="text" name="answerFalse${howMuchQuestions}"/>
    </div>`
    $('#form').append(question)
})

$('#form').submit(function (event) {
    event.preventDefault()
    console.log(event)
    const newTest = {
        title: event.target.name.value,
        questions: []
    }
    $("form#form :input").each(function () {
        const input = $(this)[0];
        if (input.name.includes("question") && input.value) {
            newTest.questions.push({question: input.value, answers: [], isSingleChoise: true})
        }
        if (input.name.includes("answerTrue") && input.value) {
            const trueQuestions = input.value.split(",").map(a => {
                if (a.trim()) {
                    return {
                        answer: a.trim(),
                        isCorrect: true
                    }
                }
            }).filter(a => a)
            if (trueQuestions.length > 1) {
                newTest.questions[newTest.questions.length - 1].isSingleChoise = false
            }
            newTest.questions[newTest.questions.length - 1].answers = [...newTest.questions[newTest.questions.length - 1].answers, ...trueQuestions]
        }
        if (input.name.includes("answerFalse") && input.value) {
            const trueQuestions = input.value.split(",").map(a => {
                if (a.trim()) {
                    return {
                        answer: a.trim(),
                        isCorrect: false
                    }
                }
            }).filter(a => a)
            newTest.questions[newTest.questions.length - 1].answers = [...newTest.questions[newTest.questions.length - 1].answers, ...trueQuestions]
        }

    });
    console.log(newTest)
    $.ajax({
        type: "POST",
        url: `http://localhost:8080/api/test/create`,
        headers: {
            "Content-Type": "application/json",
            Authorization: window.localStorage.getItem("token")
        },
        data: JSON.stringify(newTest),
        success() {
            window.location.href = "/main.html"
        },
        error(response) {
            console.log(response)
            alert(response.responseJSON.responseMessage)
        }
    })
})