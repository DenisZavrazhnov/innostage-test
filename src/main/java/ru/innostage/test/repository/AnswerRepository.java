package ru.innostage.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.innostage.test.models.Answer;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
