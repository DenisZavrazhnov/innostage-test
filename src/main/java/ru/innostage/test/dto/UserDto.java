package ru.innostage.test.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserDto {
    private long id;
    private String username;
    private long sumOfPoints;
    private long dateOfCreate;
    private boolean isAdmin;
    private long testCounter;
    private long maxPointOfPassedTests;

}
