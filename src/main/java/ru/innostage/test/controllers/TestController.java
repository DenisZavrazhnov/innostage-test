package ru.innostage.test.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.innostage.test.config.jwt.UserDetailsImpl;
import ru.innostage.test.dto.ResponseDto;
import ru.innostage.test.dto.TestDto;
import ru.innostage.test.models.Test;
import ru.innostage.test.service.TestService;

import java.util.List;

@RestController
@RequestMapping("/api/test")
@RequiredArgsConstructor
public class TestController {
    private final TestService testService;

    @GetMapping("/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Test> getTest(@PathVariable long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getDetails();
        return ResponseEntity.ok(testService.getTest(id));
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<ResponseDto> createTest(@RequestBody TestDto testDto) {
        return ResponseEntity.ok(testService.createTest(testDto));
    }

    @GetMapping("/list")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<TestDto>> getTestList() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getDetails();
        return ResponseEntity.ok(testService.getTestList(userDetails.getUserId()));
    }

    @GetMapping("/maxpoints")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Long> getMaxPoints() {
        return ResponseEntity.ok(testService.getMaxPointsForAllTests());
    }

    @PostMapping("/submit")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<ResponseDto> submitTest(@RequestBody TestDto testDto) {
        System.out.println(testDto);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getDetails();
        return ResponseEntity.ok(testService.submitTest(userDetails.getUserId(), testDto));
    }
}
